<?php

use CodeShopping\Models\Product;
use CodeShopping\Models\ProductInput;
use Illuminate\Database\Seeder;

class ProductInputTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::all();
        factory(ProductInput::class,150)->make()->each(function ($input) use ($products) {
            $input->product_id = $products->random()->id;
            $input->save();
        });
    }
}
