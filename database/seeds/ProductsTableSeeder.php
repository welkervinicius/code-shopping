<?php

use CodeShopping\Models\Category;
use CodeShopping\Models\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::all();
        factory(Product::class,50)->create()->each(function (Product $product) use ($categories) {
            $product->categories()->attach($categories->random()->id);
            rand(0,2) ?: $product->categories()->attach($categories->random()->id);
            rand(0,5) ?: $product->categories()->attach($categories->random()->id);
            rand(0,10) ?: $product->categories()->attach($categories->random()->id);
        });
    }
}
