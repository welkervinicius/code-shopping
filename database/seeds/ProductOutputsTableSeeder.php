<?php

use CodeShopping\Models\Product;
use CodeShopping\Models\ProductOutput;
use Illuminate\Database\Seeder;

class ProductOutputsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::all();
        factory(ProductOutput::class,50)->make()->each(function ($output) use ($products) {
            $output->product_id = $products->random()->id;
            $output->save();
        });
    }
}
