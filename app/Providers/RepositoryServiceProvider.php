<?php

namespace CodeShopping\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('CodeShopping\Repositories\CategoryRepository', 'CodeShopping\Repositories\CategoryRepositoryEloquent');
        $this->app->bind('CodeShopping\Repositories\ProductRepository', 'CodeShopping\Repositories\ProductRepositoryEloquent');
    }
}
