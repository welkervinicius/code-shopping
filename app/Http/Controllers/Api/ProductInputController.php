<?php

namespace CodeShopping\Http\Controllers\Api;

use CodeShopping\Http\Requests\Product\ProductInputRequest;
use CodeShopping\Http\Resources\ProductInputResource;
use CodeShopping\Models\ProductInput;
use CodeShopping\Http\Controllers\Controller;

class ProductInputController extends Controller
{
    public function index()
    {
        return ProductInputResource::collection(ProductInput::with('product')->paginate(10));
    }

    public function store(ProductInputRequest $request)
    {
        $input = ProductInput::create($request->all());
        return new ProductInputResource($input);
    }

    public function show(ProductInput $input)
    {
        return new ProductInputResource($input);
    }
}
