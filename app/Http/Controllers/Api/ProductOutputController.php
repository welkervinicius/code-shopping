<?php

namespace CodeShopping\Http\Controllers\Api;

use CodeShopping\Http\Requests\Product\ProductOutputRequest;
use CodeShopping\Http\Resources\ProductOutputResource;
use CodeShopping\Models\ProductOutput;
use Illuminate\Http\Request;
use CodeShopping\Http\Controllers\Controller;

class ProductOutputController extends Controller
{
    public function index()
    {
        return ProductOutputResource::collection(ProductOutput::with('product')->paginate(10));
    }

    public function store(ProductOutputRequest $request)
    {
        $output = ProductOutput::create($request->all());
        return new ProductOutputResource($output);
    }

    public function show(ProductOutput $output)
    {
        return new ProductOutputResource($output);
    }
}
