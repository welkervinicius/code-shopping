<?php

namespace CodeShopping\Http\Controllers\Api;

use CodeShopping\Http\Requests\Product\ProductCategoriesRequest;
use CodeShopping\Http\Resources\ProductCategoryResource;
use CodeShopping\Models\Category;
use CodeShopping\Models\Product;
use CodeShopping\Http\Controllers\Controller;

class ProductCategoryController extends Controller
{
    public function index(Product $product)
    {
        return new ProductCategoryResource($product);
    }

    public function store(ProductCategoriesRequest $request, Product $product)
    {
        $product->categories()->sync($request->categories);
        return new ProductCategoryResource($product);
    }

    public function destroy(Product $product, Category $category)
    {
        $product->categories()->detach($category->id);
        return response()->json([],204);
    }
}
