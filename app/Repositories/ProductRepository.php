<?php

namespace CodeShopping\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductRepository.
 *
 * @package namespace CodeShopping\Repositories;
 */
interface ProductRepository extends RepositoryInterface
{
    //
}
