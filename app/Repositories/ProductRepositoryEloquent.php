<?php

namespace CodeShopping\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use CodeShopping\Repositories\ProductRepository;
use CodeShopping\Models\Product;
use CodeShopping\Validators\ProductValidator;

/**
 * Class ProductRepositoryEloquent.
 *
 * @package namespace CodeShopping\Repositories;
 */
class ProductRepositoryEloquent extends BaseRepository implements ProductRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Product::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
