<?php

namespace CodeShopping\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CategoryRepository.
 *
 * @package namespace CodeShopping\Repositories;
 */
interface CategoryRepository extends RepositoryInterface
{
    //
}
