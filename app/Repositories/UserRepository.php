<?php

namespace CodeShopping\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository.
 *
 * @package namespace CodeShopping\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    //
}
