<?php

namespace CodeShopping\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use CodeShopping\Repositories\UserRepository;
use CodeShopping\Models\User;
use CodeShopping\Validators\UserValidator;

/**
 * Class UserRepositoryEloquent.
 *
 * @package namespace CodeShopping\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
